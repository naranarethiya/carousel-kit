<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Carousel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carousel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');   
            $table->string('carousel_title',220);
            $table->integer('total_items');
            $table->string('unique_code',220);
            $table->integer('option_autoplay');
            $table->integer('option_loop');
            $table->integer('option_slideBy');
            $table->timestamps();
        });

        Schema::create('carousel_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('carousel_id');   
            $table->string('title',220);
            $table->longText('caption');
            $table->longText('image');
            $table->integer('order');   
            $table->integer('enable');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
