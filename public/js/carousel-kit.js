// load jquery if not loaded
if (typeof jQuery === 'undefined') {
  	loadScript('https://code.jquery.com/jquery-3.4.1.min.js',loadAssets);
}
else {
	loadAssets();
}

// change this as per settings
var baseUrl = 'http://localhost/carousel-kit/public/';
var targetClass = '.carousel-kit';
var uniqueCodeAttr = 'data-id';
var targetDiv = '.carousel-kit['+uniqueCodeAttr+']';

var initClass = 'init';
var slideLoadedClass = 'slideLoaded';

function loadAssets() {	
	// load owl carousel
	loadCss('https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css');
	loadCss('https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css');
	loadCss('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css');
	loadScript('https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js',initCarouselKit);
}

function initCarouselKit() {
	$(targetDiv).each(function() {
		if(!$(this).hasClass(initClass)) {
			$(this).addClass(initClass);
			getCarouselItem($(this).attr(uniqueCodeAttr));
		}
	});
}

function getCarouselItem(uniqueCode) {
	var url = baseUrl+'getCarouselItem/'+uniqueCode;
	$.ajax({
		type:"POST",
		url:url,
	})
	.done(function(data) {
		$('[data-id="'+data.uniqueCode+'"]').addClass(slideLoadedClass);
		if(data.carouseItem.length > 0) {
			prepaireCarousel(data.uniqueCode, data.carouseItem, data.carousel)
		}
	})
	.fail(function(jqXHR, textStatus) {
		console.log( "Request failed: " + textStatus );
	})
	.always(function() {
		
	});
}


function prepaireCarousel(uniqueCode, items, carouselOptions) {
	var appendHtml = '<div class="owl-carousel owl-theme">';
	$.each(items, function(key, item) {
		appendHtml+='<div class="item"><img src="'+baseUrl+item.image+'"></div>';
	});
	appendHtml+='</div>'
	$div = $(targetClass+'['+uniqueCodeAttr+'="'+uniqueCode+'"]');
	$div.html(appendHtml);
	
	$('.owl-carousel',$div).owlCarousel({
		loop:carouselOptions.option_loop,
		autoplay:carouselOptions.option_autoplay,
		slideBy:carouselOptions.option_slideBy,
		margin:10,
		responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});
}

function loadScript(src,onload) {
	var headTag = document.getElementsByTagName("head")[0];
    var jqTag = document.createElement('script');
    jqTag.type = 'text/javascript';
    jqTag.src = src;
    if(onload) {
    	jqTag.addEventListener('load', onload);
    }
    headTag.appendChild(jqTag);
}

function loadCss(href) {
	var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    link.media = 'all';
    head.appendChild(link);
}