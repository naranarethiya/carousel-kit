$(document).ready(function() {
	
});



function load_popup(title,data, modal) {
	if(!modal) {
		modal='#comman-modal';
	}

	$(modal+' .modal-title').html(title);
	$(modal+' .modal-body').html(data);
	$(modal).modal('show');
}

function get_modaldata(url,title, modal) {
	var token = $('meta[name="csrf-token"]').attr('content');
	$.ajax({
		type:"POST",
		url:url,
		data:{'_token':token},
		beforeSend:function() {
			loading();
		}

	})
	.done(function(data) {
		remove_loading();
		load_popup(title,data,modal);
	})
	.fail(function(jqXHR, textStatus) {
		remove_loading();
		alert( "Request failed: " + textStatus );
	})
	.always(function() {
		remove_loading();
	});
}

/* ajax waiting */
function loading() {
	over='<div class="loading-overlay" style="position: fixed;left: 0;top: 0;bottom: 0;right: 0;background: #000;opacity: 0.8;filter: alpha(opacity=80);z-index: 9999;"><i style="position: absolute;top: 49%;left: 49%;font-size:37px;color:#fff;-webkit-animation: fa-spin 2s infinite linear;animation: fa-spin 2s infinite linear;" class="fa fa-refresh"></i></div>';
	$('body').append(over);
}

function remove_loading() {
	$('.loading-overlay').remove();
}