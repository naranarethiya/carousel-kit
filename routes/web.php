<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("login");
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//carousel methods
Route::get('carousel', 'CarouselController@index')->name('carousel');
Route::any('carousel/carouselAdd/{id?}', 'CarouselController@carouselAdd')->name('carouselAdd');
Route::post('carousel/carouselSave', 'CarouselController@carouselSave')->name('carouselSave');
Route::get('carousel/carouselDetail/{id}', 'CarouselController@carouselDetail')->name('CarouselDetail');
Route::any('carousel/carouselSnippet/{id}', 'CarouselController@carouselSnippet')->name('carouselSnippet');

// carouse items
Route::any('carousel/itemAdd/{carousel}/{id?}', 'CarouselController@itemAdd')->name('itemAdd');
Route::post('carousel/itemSave', 'CarouselController@itemSave')->name('itemSave');
Route::post('carousel/itemReorder', 'CarouselController@itemReorder')->name('itemReorder');


Route::any('getCarouselItem/{uniqueCode}', 'WidgetController@getCarouselItem');
