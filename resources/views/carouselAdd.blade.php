<form method="post" action="{{route('carouselSave')}}">
	@csrf

	@if(!empty($editData))
		<input type="hidden" name="id" value="{{$editData->id}}">
	@endif
	
	<div class="row">
		<div class="col-sm-12">

			<div class="form-group">
			  	<label>Title</label>
			  	<input type="text" name="carousel_title" class="form-control" placeholder="Carousel title" required
			  		value="@if(!empty($editData)) {{$editData->carousel_title}} @endif" 
			  	>
			  	<p class="help-block">For your reference only, its won't display on widget.</p>
			</div>
			<div class="clearfix"></div>
			<h3>Carouse Options</h3>

			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
					  	<label>Autoplay</label>
					  	<select class="form-control" name="option_autoplay" required>
					  		<option value="1" @if(!empty($editData) && $editData->option_autoplay=='1') selected @endif >Yes</option>
					  		<option value="0" @if(!empty($editData) && $editData->option_autoplay=='0') selected @endif >No</option>
					  	</select>
					  	<p class="help-block">Autoplay Carousel.</p>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
					  	<label>Loop Slides</label>
					  	<select class="form-control" name="option_loop" required>
					  		<option value="1" @if(!empty($editData) && $editData->option_loop=='1') selected @endif>Yes</option>
					  		<option value="0" @if(!empty($editData) && $editData->option_loop=='0') selected @endif>No</option>
					  	</select>
					  	<p class="help-block">Infinity loop. Duplicate last and first items to get loop illusion.</p>
					</div>
				</div>


				<div class="col-sm-6">
					<div class="form-group">
					  	<label>Slide By</label>
					  	<input type="number" name="option_slideBy" class="form-control" placeholder="Number of slide" value="1" required
					  		value="@if(!empty($editData)) {{$editData->option_slideBy}} @endif" 
					  	>
					  	<p class="help-block">How many items to be slide at a time.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="modal-footer">	
					<input type="submit" value="Submit" class="btn btn-primary pull-right">
				</div>
			</div>

		</div>
	</div>

</form>