@extends('layouts.admin')
@section('content')


<div class="content">
    <div class="row">
        <div class="col-lg-12">
        	<a href="{{route('carousel')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
            <h3>Carousel : {{$carousel->carousel_title}}</h3>
        </div>

        <div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a onclick="get_modaldata('{{route('itemAdd',['carousel'=>$carousel->id])}}','Add Carousel Item')" class="btn btn-sm btn-primary" title="Add New Item">
							<i class="fa fa-plus"></i> Add New Item
						</a>
						<a onclick="get_modaldata('{{route('carouselAdd',['id'=>$carousel->id])}}','Edit carousel detail')" class="btn btn-sm btn-primary" title="Edit Carouse">
							<i class="fa fa-pencil"></i> Edit Carouse
						</a>

						<a onclick="get_modaldata('{{route('carouselSnippet',['id'=>$carousel->id])}}','Carousel Snippet')" class="btn btn-sm btn-primary" title="Get code Carouse">
							<i class="fa fa-code"></i> Get code
						</a>
					</div>
				</div>

				<div class="panel-body table-responsive">

					<table class='table table-bordered table-striped'>
						<thead>
							<tr>
								<th>Order</th>
								<th>Title</th>
								<th>Caption</th>
								<th>Image</th>
								<th>Enable</th>
								<th>Created At</th>
								<th width="5%">Action</th>
							</tr>
						</thead>
                
						<tbody> 
							@foreach ($carouselItem as $item)

							<tr>
								<td>{{$item->order}}</td>
								<td>{{$item->title}}</td>
								<td>{{$item->caption}}</td>
								<td>
									<img src="{{asset($item->image)}}" style="width: 100px">
								</td>
								<td>
									@if($item->enable == 1)
										<small class="label bg-green">Enable</small>
									@else
										<small class="label bg-danger">Disable</small>
									@endif
								</td>
								<td>{{$item->created_at}}</td>
								<td>
									<div class="btn-group options">
										<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fa fa-cog"></i> Options
										</button>
										 
										<ul class="dropdown-menu">
											<li>
												<a onclick="get_modaldata('{{route('itemAdd',['carousel'=>$carousel->id,'id'=>$item->id])}}','Edit Item')">
													<i class="fa fa-pencil"></i> Edit
												</a>
											</li>
										</ul>
									</div>
								</td>
								
							</tr>

							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
@parent

@endsection