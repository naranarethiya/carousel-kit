<div class="row">
	<div class="col-sm-12">

		<div class="form-group">
		  	<label>HTML code</label>
		  	<textarea disabled class="form-control"><div class="carousel-kit" data-id="{{$carousel->unique_code}}"></div>
		  	</textarea>
		  	<p class="help-block">
		  		Place this code wherever you want the carousel to appear on your page.
		  	</p >
		</div>

		<div class="form-group">
		  	<label>Javascript code</label>
		  	<textarea disabled class="form-control"><script crossorigin async src="{{ asset('js/carousel-kit.js') }}"></script>
		  	</textarea>
		  	<p class="help-block">
		  		Include the JavaScript on your page once, ideally right after the opening body tag.
		  	</p>
		</div>
	</div>
</div>