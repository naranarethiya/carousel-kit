@extends('layouts.admin')
@section('content')


<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <h3>Carousel List</h3>
        </div>

        <div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a onclick="get_modaldata('{{route('carouselAdd')}}','Add New carousel',)" class="btn btn-sm btn-primary" title="Add New"><i class="fa fa-plus"></i> Add Carousel</a>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped'>
							<thead>
								<tr>
									<th>Title</th>
									<th>Total Items</th>
									<th>Unique Code</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
                    
							<tbody> 
								@foreach ($carouselList as $carousel)
								<tr>
									<td>
										<a href="{{route("CarouselDetail",["id"=>$carousel->id])}}">{{$carousel->carousel_title}}</a>
									</td>
									<td>{{$carousel->total_items}}</td>
									<td>{{$carousel->unique_code}}</td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
												<li>
													<a onclick="get_modaldata('{{route('carouselAdd',['id'=>$carousel->id])}}','Edit carousel detail')">
														<i class="fa fa-pencil fa-margin"></i> Edit
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-plus fa-margin"></i> Add Slide
													</a>
												</li>
												<li>
													<a onclick="get_modaldata('{{route('carouselSnippet',['id'=>$carousel->id])}}','Carousel Snippet')">
														<i class="fa fa-plus fa-margin"></i> Get Code
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>


    </div>
</div>

@endsection
@section('scripts')
@parent

@endsection