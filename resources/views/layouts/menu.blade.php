<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu tree" data-widget="tree">
            <li>
                <a href="{{ route("carousel") }}">
                    <i class="fas fa-fw fa-tachometer-alt">
                    </i>
                    Dashboard
                </a>
            </li>
            
            <!-- <li class="treeview">
                <a href="#">
                    <i class="fa-fw fas fa-users">

                    </i>
                    <span>Menu one</span>
                    <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="#">
                            <i class="fa-fw fas fa-unlock-alt"></i>
                            <span>Menu 2</span>
                        </a>
                    </li>
                </ul>
            </li> -->
            <li>
                <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="fas fa-fw fa-sign-out-alt">

                    </i>
                    Logout
                </a>
            </li>
        </ul>
    </section>
</aside>