<form method="post" action="{{route('itemSave')}}" enctype="multipart/form-data">
	@csrf

	@if(!empty($editData))
		<input type="hidden" name="id" value="{{$editData->id}}">
	@endif

	<input type="hidden" name="carousel_id" value="{{$carousel_id}}">
	
	<div class="row">
		<div class="col-sm-12">

			<div class="form-group">
			  	<label>Title</label>
			  	<input type="text" name="title" class="form-control" placeholder="Item title" required
			  		value="@if(!empty($editData)) {{$editData->title}} @endif" 
			  	>
			</div>
			<div class="clearfix"></div>

			<div class="form-group">
			  	<label>Caption</label>
			  	<input type="text" name="caption" class="form-control" placeholder="Item Caption" required
			  		value="@if(!empty($editData)) {{$editData->caption}} @endif" 
			  	>
			</div>

			<div class="form-group">
				<label>Image</label>
			  	<input type="file" name="image" class="form-control" accept="image/*"
			  		value="@if(!empty($editData)) {{$editData->caption}} @endif"
			  		@if(empty($editData)) required  @endif
			  	>
			</div>

			<div class="clearfix"></div>
			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
					  	<label>Enable</label>
					  	<select class="form-control" name="enable" required>
					  		<option value="1" @if(!empty($editData) && $editData->enable=='1') selected @endif >Enable</option>
					  		<option value="0" @if(!empty($editData) && $editData->enable=='0') selected @endif >Disable</option>
					  	</select>
					  	<p class="help-block">Disabled items won't display on widget.</p>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
					  	<label>Slide Number</label>
					  	<input type="number" name="order" class="form-control" placeholder="Number of slide" required
					  		value="@if(!empty($editData)){{$editData->order}}@else{{$carousel->total_items+1}}@endif" 
					  	>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="modal-footer">	
					<input type="submit" value="Submit" class="btn btn-primary pull-right">
				</div>
			</div>

		</div>
	</div>

</form>