<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Carousel extends Model
{
    //
    protected $table = 'carousel';

    public function carouselItem()
    {
        return $this->hasMany('App\CarouselItem')->orderBy('order','asc');
    }

    public function carouseEnablelItem()
    {
        return $this->hasMany('App\CarouselItem')->where('enable','1')->orderBy('order','asc');
    }

    public static function getUniqueToken() {
    	$uniqueCode = Str::random(10);
        $duplicateUniqueCode = 1;

        //generate unique_code by checking in database
        while ($duplicateUniqueCode == 1) {
            $carouseDuplicate = Carousel::where('unique_code',$uniqueCode)
            ->first();
            if(empty($carouseDuplicate)) {
                $duplicateUniqueCode = 0;
            }
            else {
                $uniqueCode = Str::random(10);
            }
        }
        return $uniqueCode;
    }

}
