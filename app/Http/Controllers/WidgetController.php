<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;
use App\CarouselItem;

class WidgetController extends Controller
{
    //
    public function __construct() {
	    $this->middleware('cors');
	}

	public function getCarouselItem($uniqueCode) {
		$carousel = Carousel::where('unique_code', $uniqueCode)
                    ->first();	

		if(empty($carousel)) {
			return [];
		}

		$data['uniqueCode'] = $uniqueCode;
		$data['carousel'] = $carousel;
		$data['carouseItem'] = Carousel::find($carousel->id)
						->carouseEnablelItem()
						->get();
		return response($data);
	}
}
