<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;
use App\CarouselItem;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;





class CarouselController extends Controller
{
    //
    public function __construct() {
	    $this->middleware('auth');
	}

	public function index() {
		$viewData['carouselList'] = Carousel::where('user_id', Auth::id())->get();
		return view('carousel',$viewData);
	}

	public function carouselAdd($id = false) {
		$viewData = [];
        if($id) {
            $viewData['editData'] = Carousel::where('id',$id)->first();
        }
		return view('carouselAdd',$viewData);
	}

	public function carouselSave(Request $request) {
		$input = $request->all();

		$validator = Validator::make($request->all(), [
            'carousel_title' => 'required',
            'option_autoplay' => 'required',
            'option_loop' => 'required',
            'option_slideBy' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('carousel')
                    ->withErrors($validator)
                    ->withInput();
        }

        // update request
        if ($request->filled('id')) {
            $update_array = [
                'carousel_title'=>$request->carousel_title,
                'option_autoplay'=>$request->option_autoplay,
                'option_loop'=>$request->option_loop,
                'option_slideBy'=>$request->option_slideBy,
            ];

            Carousel::where('id',$request->id)
                    ->where('user_id',Auth::id())
                    ->update($update_array);

            return redirect()->back()
                    ->with("message","Carouse detail updated.");
        }
        // add request
        else {
            $carousel = new Carousel;
            // create new 
            $carousel->carousel_title = $request->carousel_title;
            $carousel->user_id = Auth::id();
            $carousel->unique_code = Carousel::getUniqueToken();

            $carousel->option_autoplay = $request->option_autoplay;
            $carousel->option_loop = $request->option_loop;
            $carousel->option_slideBy = $request->option_slideBy;
            $carousel->total_items = 0;
            $carousel->save();
            
            return redirect()->action('CarouselController@carouselDetail', ['id' => $carousel->id])
            ->with("message","New Carousel created, Add Carousel items to make it perfect.");
        }
	}

	public function carouselDetail($id) {
		$viewData['carousel'] = Carousel::where('user_id', Auth::id())
                            		->where('id',$id)
                            		->first();

        // prevent unauthorizes access 
        if(empty($viewData['carousel'])) {
            abort(404);
        }

        $viewData['carouselItem'] = Carousel::find($id)->carouselItem;

		return view('carouselDetail',$viewData);
	}

    public function carouselSnippet($id) {
        $viewData['carousel'] = Carousel::where('id',$id)
                    ->where('user_id',Auth::id())
                    ->first();

        if(empty($viewData['carousel'])){
            return abort(404);
        }

        return view('carouselSnippet',$viewData);

    }

	public function itemAdd($carouselId,$id = false) {
		$viewData = [
            'carousel_id'=>$carouselId
        ];

        $viewData['carousel'] = Carousel::where('id',$carouselId)
                    ->where('user_id',Auth::id())
                    ->first();

        if(empty($viewData['carousel'])){
            return abort(404);
        }

        if($id) {
            $viewData['editData'] = CarouselItem::where('id',$id)->first();
        }
        return view('carouselItemAdd',$viewData);
	}

	public function itemSave(Request $request) {
        $validationRules = [
            'carousel_id' =>'required',
            'title' => 'required',
            'enable' => 'required',
            'order' => 'required',
        ];

        // ignore image validation for edit
        if (!$request->filled('id')) {
            $validationRules['image'] = 'required|image';
        }

        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        // verify if carousel_id is of same user
        $carousel = Carousel::where('id',$request->carousel_id)
                            ->where('user_id',Auth::id())
                            ->first();

        if(empty($carousel)){
            return redirect()->back()
                ->with("errorMessage","Provided Carouse is not belong to current user");
        }

        // upload image
        $imagePath = '';
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $imagePath = $request->image->store('carousel-images');
        }

        // update request
        if ($request->filled('id')) {

            $updateArray = [
                'title'=>$request->title,
                'caption'=>$request->caption,
                'order'=>$request->order,
                'enable'=>$request->enable,
            ];


            // if new image uploaded
            if(!empty($imagePath)) {
                $updateArray['image'] = $imagePath;
            }

            CarouselItem::where('id',$request->id)
                    ->update($updateArray);

            return redirect()->back()
                    ->with("message","Item detail updated.");
        }
        // add request
        else {
            $carouselItem = new CarouselItem;
            // create new 
            $carouselItem->carousel_id = $request->carousel_id;
            $carouselItem->title = $request->title;
            $carouselItem->caption = $request->caption;
            $carouselItem->order = $request->order;
            $carouselItem->enable = $request->enable;
            $carouselItem->image = $imagePath;
            $carouselItem->save();

            // update carouse item count
            DB::update('update carousel set total_items = total_items+1 where id = ?', [$request->carousel_id]);
            
            return redirect()->back()
            ->with("message","New Item added successfully");
        }
	}
}
